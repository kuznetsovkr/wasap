import project


def test_second_function():
    v1 = [6, 3, 4]
    v2 = project.second_function(v1)
    assert v2 == 1


def test_fourth_function():
    v1 = [6, 3, 4, 4, -3]
    v2 = project.fourth_function(v1)
    assert v2 == 2


def test_fifth_function():
    v1 = [6, 3, 4, 4, -3]
    v2 = project.fifth_function(v1)
    assert v2 == [6, 4, 3]


def main():
    test_second_function()
    test_fourth_function()
    test_fifth_function()


if __name__ == "__main__":
    main()


